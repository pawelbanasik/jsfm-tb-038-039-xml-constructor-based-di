package com.pawelbanasik.domain;

import com.pawelbanasik.service.BusinessService;

public class Organization {

	private String companyName;
	private int yearOfIncorporation;
	private String postalCode;
	private String employeeCount;
	private String slogan;
	private BusinessService businessService;
	
	
//	public Organization() {
//
//	}

	public Organization(String companyName, int yearOfIncorporation) {
		this.companyName = companyName;
		this.yearOfIncorporation = yearOfIncorporation;
	}

//	public void corporateSlogan() {
//		String slogan = "We build the ultimate driving machines";
//		System.out.println(slogan);
//	}
	
	// replacing the above method with this one
	public String corporateSlogan() {
		return slogan;
	}

	// accesses the buisness service and executes the office service methods 
	// after the injection of the desired service implementation
	public String corporateService() {
		return businessService.offeringService(companyName);
	}
	
	@Override
	public String toString() {
		return "Organization [companyName=" + companyName + ", yearOfIncorporation=" + yearOfIncorporation
				+ ", postalCode=" + postalCode + ", employeeCount=" + employeeCount + "]";
	}


	public void setEmployeeCount(String employeeCount) {
		this.employeeCount = employeeCount;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	// removes the hard coding (has nothing to do with DI)
	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}

	// this will help us do the dependency injection along with some configuration
	// spring config file
	// you need this setter to create property in the xml file for beans
	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}

	public String getCompanyName() {
		return companyName;
	}

	
	
}

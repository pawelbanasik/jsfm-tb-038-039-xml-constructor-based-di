package com.pawelbanasik.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.pawelbanasik.domain.HumanResourceDept;
import com.pawelbanasik.domain.Organization;

public class DIConstructorApp {
	public static void main(String[] args) {

		ApplicationContext context = new FileSystemXmlApplicationContext("beans.xml");
		
		Organization organization = (Organization) context.getBean("myorg");
		
		System.out.println(organization.corporateSlogan());

//		System.out.println(organization);
		
//		System.out.println(organization.corporateService());
		
		HumanResourceDept hrdept = (HumanResourceDept) context.getBean("myhrdept");
		
		System.out.println(hrdept.hiringStatus(5500));
		
		((AbstractApplicationContext) context).close();
		
	}
}
